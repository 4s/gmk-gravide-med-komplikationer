# GMK Backend Architecture 

[TOC]

## Introduction
This page describes the design and architecture of the backend part of the [GMK project](README.md). 

Notice that the backend can also be referred to as _Health & Fitness Service (H&FS)_ when speaking in terms of the [Continua Health Guidelines](https://www.pchalliance.org/continua-design-guidelines)
or as _Central Opsamlingspunkt_ when speaking in terms of the [Danish reference architectures for health IT](https://sundhedsdatastyrelsen.dk/da/rammer-og-retningslinjer/om-referencearkitektur-og-standarder/referencearkitekturer)

On the backend the project develops loosly couply microservices for converting, 
storing, processing and exhibiting the incoming data from Personal Health Gateway devices (see [GMK Architecture](README.md))
and for the overall setup and handling of patients in telemedical care. Furthermore, the project develops components on 
the backend for communicating data to other, downstream health IT systems.

![H&FS - overall architecture](HFS_overall_architecture.png)

The backend business functionality is exposed to external clients (apps) through patient and employee (health 
professional) backend-for-frontend (BFF) services and through an input service for receiving questionnaire responses 
and measurement results. BFFs and input services encapsulate the central microservice-based infrastructure. The 
microservices inside the backend convert, store, process and exhibit incoming data. In addition, separate microservices 
provide the forwarding of the incoming data to downstream health IT systems.

Notice that XDS archives are represented in the illustration above. In this project it is not part of the work carried
 out by the Alexandra Institute to integrate with XDS. Conversion services (see below), that are part of this project, 
 will deliver CDA format documents on the asynchronous messaging system and these can be picked up by any service 
 implementing XDS repository integration.

## Components of a microservice
__Docker__:Each microservice is packaged in a Docker container. This amongst others ensure isolation of processing from other 
microservices and enables easy provisioning and deployment of individual microservices. 

__REST__: Services that expose RESTful endpoints will run web service inside an application server / servlet container. In GMK 
this is mostly done using  [Jetty](https://www.eclipse.org/jetty/). If the web service exposes FHIR RESTful endpoints
we typically build this functianality on top of a version of the [HAPI FHIR server](https://hapifhir.io/).

__Messaging__: Services that consume asynchronous messages do so using a Kafka consumer that subscribes to designated
Kafka topics. When consuming messages from Kafka a service will only commit the offset of the current message after
it's done processing the current message. This means that if the service breaks down mid processing of a message it will  
reprocess the message when the service is brought back up. Services that produce asynchronous messages do so using a 
Kafka producer that emits messages to designated Kafka topics. See 
the [documentation of the messaging library](https://bitbucket.org/4s/messaging/src/master/docs/Main.md) for details 
regarding topic and message structure.

Services that only use Kafka for communication do not run as web services inside a servlet container but are run as
simple processes (f.ex. implemented in Java) inside their Docker container. 

__Logging & monitoring__:All services produce JSON formated log output that f.ex. can be forwardet, index and visualized via the 
[ELK](https://www.elastic.co/what-is/elk-stack) tool chain. Services can be configured though their environment to use
one of the following logging levels: TRACE, DEBUG, INFO, WARN and ERROR. At TRACE level should only be used in 
non-production settings as patient sensitive information may be logged as this level. At DEBUG level or above no person
sensitive information should be logged. 

Services that are implemented as web services expose a RESTful endpoint with metrics that can be queried
by a Prometheus instance. Services that are purely Kafka based can be setup to forward metrics to a Prometheus Push
Gateway.

__Environment__: All service read environment variables for setup information on such things as RESTful service 
addresses (used when querying other services), Kafka setup, authentication configuration, logging level and FHIR
coding system information.

## Types of microservices
Conceptually we can divide our microservices into the following overall categories:

  - Backend-for-frontends
  - Input services
  - Stateful business services
  - Stateless computation services
  
### Backend-for-frontends

A backend-for-frontend (BFF) is where you assemble and expose the business functionality that targets a specific type of
client application. The typical alternative to a BFF strategy will either be to have a single server-side API exposed to all clients of all
types, or alternatively to let clients have a deep knowledge of the internal microservice infrastructure and call them
directly through an API gateway. The latter option is generally not desirable, as all external clients thus become
hardwired in the internal microservice infrastructure – i.e. changes to a single microservice's API may cause all
clients to change. The first option typically has disadvantages in those situations where client apps run on different
types of platforms with different interface and bandwidth requirements. In our case, the requirements for interface,
bandwidth and functionality depend on whether it is aimed at patients or at health professionals and we thus have a patient
BFF and an employee BFF.

See the [documentation for the employee BFF](https://bitbucket.org/4s/employee-bff/src/master/docs/Main.md) for 
details on how BFFs wait for / time out on answers on asynchronous updates. 

Conceptual structure of a backend-for-frontend service:
 
![Structure of a backend-for-frontend service](bff_and_input.png)
 
Illustration of BFF versus general purpose server-side API from http://samnewman.io/patterns/architectural/bff/:

![Sam Newman - Pattern: Backends For Frontends](BFF_Sam_Newman.png)

### Input services

Input services expose RESTful endpoints to citizen's apps and devices. This is where these apps and devices deliver their data
in different formats. Input services are like BFFs living on the edge of the intrastructure, between apps and the
internal microservice infrastructure. In a sense, they can be seen as part of the BFF strategy, but they are implemented
as separate services: One for each type of input that is to be supported. Input that can be received by an input service
is typically data related to measurements, observations and questionnaire responses pertaining to a single citizen. Each
input service will typically expose interface for receiving data in a specific format. For instance, in our case the
backend has a single input service exposing interfaces for receiving data in HL7 FHIR format. Other examples of input
services could be ones that receives input in MQTT or HL7 PCD format. 

When receiving input an input-service may query other microservices for data that is used to f.ex. enhance 
or verify the input. An input service will end it's processing of data by outputting (potentially enhanced) received input
to the asynchronous messaging system.  

_Example_: The [fhir-input-service](https://bitbucket.org/4s/fhir-input-service) is an input service that exposes a FHIR RESTful interface for receiving questionnaire 
responses and measurements from patients.

Conceptual structure of an input service:
 
![Structure of an input service](bff_and_input.png)

### Stateful business services
Stateful business services handle business functionality in relation to a subset of business resources. Stateful services 
persist their resources in an underlying database. Stateful services expose RESTful endpoints for querying the state of 
resources and they subscribe to create, update and delete messages from the underlying asynchronous messaging system 
(also see the section on _Communication_). After processing a create, update or delete message a stateful service will
issues one or more messages to the messaging system. Such messages will typically contain an event describing what has 
happened (f.ex. an "updated" or "created" event) and the data that was processed.

During the processing of queries and commands stateful business services may query other microservices for data that is 
used during for the processing of the query or command.

_Example_: The [organizational-service](https://bitbucket.org/4s/organizational-service) is a stateful business service
 that handles the business functionality pertaining to FHIR Organization, Practitioner and PractitionerRole resources.
 It exposes FHIR RESTful interfaces for querying those resource types. It subscribes to Kafka messages containing
 commands to create, update and delete those resource types.
 
 Conceptual structure of a stateful business service:
 
 ![Structure of a stateful business service](stateful_business_service.png)

### Stateless computation services
Stateless computation services receive input over the asynchronous messaging system, do some data processing/enhancement/qualification
on the data and then output the result over the messaging system again. Example tasks carried out by a stateless computation
service could be conversion or transcoding of data.

During the processing of events and commands stateful business services may query other microservices for data that is 
used during for the processing of the event or command. 

_Example_: [triaging-service](https://bitbucket.org/4s/triaging-service) takes incoming FHIR questionnaire responses and 
measurements and uses the thresholds in a FHIR ThresholdSet to compute an interpretation of the values of the incoming 
FHIR Observation or QuestionnaireResponse resource as either "normal", "abnormal", or "pathological". The interpretation
is appended to the Observation or QuestionnaireResponse resource and an update message containing the resource is output 
to the Kafka messaging system.

 Conceptual structure of a stateless computation service:
 
 ![Structure of a stateless computation service](stateless_computation_service.png)
 
## Communication

__Communication from the outside in:__

When interacting with the backend from the outside, that is typically when patient or employee clients interact with 
patient BFF / input services and employee BFF, communication is synchronous, RESTful communication based on HL7 FHIR.

See the [documentation for the employee BFF](https://bitbucket.org/4s/employee-bff/src/master/docs/Main.md) for 
details on how BFFs wait for / time out on answers on asynchronous updates. 

__Communication within the backend:__

Within the backend BFFs and input services (a form of microservices themselves) communicate with the microservices
inside the backend and microservices communicate with each other. Such interaction can happen in two ways, depending on
the overall nature of the communication:
 
   - Queries (read requests) are handled via synchronous RESTful communication based on FHIR
   - Commands (update, create and delete requests) are handled via asynchronous messaging over Apache Kafka  

See the [documentation of the messaging library](https://bitbucket.org/4s/messaging/src/master/docs/Main.md) for 
details regarding topic and message structure.

__Communication to downstream systems__:

When integration with external, downstream systems we typically encapsulate the integration in an ambassador or adapter 
microservice. To the microservice world on the inside of the backend such a service has a simple and standardised 
interface, while interacting with proprietary and/or legacy protocols and interfaces towards the external, downstream 
systems. 

## Security
Services can be setup to function in different security contexts. The approach is based on the concept of interceptors
that can be setup to intercept all in- and outgoing communication. With FHIR RESTful communication HAPI FHIRs 
interceptors are used and with asynchronous messaging an interceptor concept implemented in the
[messaging library](https://bitbucket.org/4s/messaging) is used. 

Incoming communication is intercepted before any other processing takes place. The first thing that happens when 
communication is intercepted is that the interceptor will attempt to retrieve a user id using a so called user context
resolver. The user context resolver will have different implementations depending on the environment in which the service
is run. F.ex. if the service is run on the DIAS platform of the Central Denmark Region the context resolver will pick up
a session id from the header of the incoming http message and use that when querying a sidecar service for a JSON object
describing the user context (user id etc.). If no user id can be resolved by the user context resolver an error will 
be thrown and no further processing will happen. Depending on the security context further processing can happen in the
interceptor, like getting a security token from the communication header, via sidecar services or similar. Once all this
is done normal proessing proceeds. 

Any outgoing communication is also intercepted. Here the communication will typically be intercepted to add security
information (session ids, tokens, ...) to the header of the outgoing message and/or to call outgoing sidecar service to
adorn them with security information.

## Microservices and supporting libraries
The following represents an summary list of the microservices and supporting libraries being developed as part of the backend (with 
links to documentation of the individual components, in so far as it is available):

  - Backend-for-frontends and input services:
    * [employee-bff](https://bitbucket.org/4s/employee-bff): Exposes FHIR RESTful interfaces for the business logic 
    for healthcare professionals implemented by backend services.
    * [patient-bff](https://bitbucket.org/4s/patient-bff): Exposes FHIR RESTful interfaces for the business 
    logic for patients implemented by backend services.
    * [fhir-input-service](https://bitbucket.org/4s/fhir-input-service): Exposes a FHIR 
    RESTful interface for receiving 
    questionnaire responses and measurements from patients. The API takes departure in Continua Design Guidelines.
  - Stateful business services:
    * [organizational-service](https://bitbucket.org/4s/organizational-service): Service handling business functionality for FHIR Organization, Practitioner and 
    PractitionerRole resources
    * [outcomedefinition-service](https://bitbucket.org/4s/outcomedefinition-service): Service handling business functionality for FHIR Questionnaire, 
    ObservationDefinition, ThresholdSet (extionsion) and Task (notifaction tasks) resources
    * [outcome-service](https://bitbucket.org/4s/outcome-service): Service handling business functionality for FHIR QuestionnaireResponse, Observation, 
    Device and Task (review tasks) resources
    * [patientcare-service](https://bitbucket.org/4s/patientcare-service): Service handling business functionality for FHIR Patient and CarePlan resources
    * [plandefinition-service](https://bitbucket.org/4s/plandefinition-service): Service handling business functionality for FHIR PlanDefinition resources
  - Stateless computation services:
    * [triaging-service](https://bitbucket.org/4s/triaging-service): Triages incoming questionnaire responses and 
    measurements. I.e. uses FHIR ThresholdSet (extension) resources to triage FHIR QuestionnaireResponse and 
    Observation resources. 
    * [FHIRQR2QRD-service](https://bitbucket.org/4s/fhirqr2qrd-service): Converts FHIR QuestionnaireResponse resources 
    into HL7 QRD format
    * [fhirObs2PHMR-service](https://bitbucket.org/4s/fhirobs2phmr-service): Converts FHIR Observation resources into 
    HL7 PHMR format
    * [FHIR2milou-service](https://bitbucket.org/4s/fhir2milou-service): Processes FHIR Bundles containing CTG data 
    and sends the extracted data to a Medexa Milou server.
  - Supporting libraries:
    * [messaging](https://bitbucket.org/4s/messaging): Library that supports sending and receiving messages over a 
    messaging system. Currently implements messaging via Apacha Kafka.
    * [fhir-resource-service-common](https://bitbucket.org/4s/fhir-resource-service-common): Project contains parent 
    Maven POM to be used for projects that build microservices that constitute a FHIR resource server.
    * [generic-resource-service](https://bitbucket.org/4s/generic-resource-service): Hapi FHIR JPA microservice template / super classes
    
### Graphical overview of microservices

![H&FS - microservices](HFS_architecture_microservices.png)
