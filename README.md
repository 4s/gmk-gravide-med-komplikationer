# GMK - Architecture

[TOC]

## Introduction
These pages contain the overall technical documentation for the project that develops components supporting 
telemedicine for women with complicated pregnancies - in Danish: "Gravide Med Komplikationer (GMK)". The project is 
funded by the Central Denmark Region and development is carried out by the Alexandra Institute.

This repository contains the overall technical design and architectural documentation for the project as well as an 
overview of the constituent components.  

This page aims to delineate the overall principles of the architecture and the context in which the components 
being developed in this project exist. 

## Design Principles
The project encourages the following design principles:

  - Loose coupling and high cohesion - applying the single responsibility principle to services.
  - Separate frontends from backend data services.
  - Decentralized data management: A data store is only to be owned by one service. Other services may only get access 
to its data through the owner's service API.
  - Small number of ways for components to interact - well described and standardised.
  - Componentisation via services - when modularising aim towards components as self-contained services interacting at 
 runtime.

## Overall business architecture
The components developed in this project are targeted for an overall business architecture as illustrated here:

![Overall business architecture](overall_business_architecture.png)

Using terms from [Continua Health Guidelines](https://www.pchalliance.org/continua-design-guidelines) this project 
develops components for Personal Health Gateway (DK: "lokal opsamlingsenhed") and Health & Fitness Service 
(DK: "centralt opsamlingspunkt"). Notice: Don't be confused by the "Service" in the latter term. In our case the 
Health & Fitness Service is made up of an infrastructure with a number of microservices, an asynchronous messaging system, 
logging and monitoring systems and more.

The Personal Health Gateway (PHG) lives on a mobile device close to the patient. In the GMK project the mobile platform 
is an Android tablet. On the PHG the project develops loosly coupled modules for communicating with measurement devices, 
for answering questionnaires and for delivering measurement results and questionnaire responses to the backend. As part of 
the PHG infrastructure the project develops a so-called baseplate for cross programming language communiction between modules. 

The Health & Fitness Service is better known simply as _the backend_ and this is term mostly used throughout these documents.
On the backend the project develops loosly coupled microservices for converting, storing, processing and exhibiting the 
incoming data from PHG devices and for the overall setup and handling of patients in telemedical care. Furthermore, the 
project develops components on the backend for communicating data to other, downstream health IT systems.

Project focus areas are illustrated in the following figure:

![Overall components of the system](overall_components.png)

That is, the PHG modules of this project are designed to be assembled with a citizen oriented app  that delivers measurement 
results and questionnaire responses to the backend through an input service and communicates with a Patient oriented 
backend-for-frontend for such things as retrieval of questionnaires to fill out and for retrieval of historic measurement results.
 
The backend business functionality is exposed to external clients (apps) through patient and employee (health 
professional) backend-for-frontend (BFF) services, and through an input service that takes care of receiving questionnaire 
responses and measurement results. BFFs and input services encapsulate the central microservice-based infrastructure. The 
microservices inside the backend convert, store, process and exhibit incoming data. In addition, microservices provide the 
forwarding of the incoming data to downstream health IT systems.

## Backend architecture
See the separate [GMK Backend Architecture document](HFS.md) describing this.

## Personal Health Gateway Architecture
Details on the Personal Health Gateway architecture is documented in [4S PHG Core library documentation](https://www.4s-online.dk/PHG/core/latest/interface/index.html).

## Standards
The project encourages use of, preferably international, standards where ever it makes sense. Some of the main 
standards and guidelines used in the project are:
 
  - [Continua Health Guidelines](https://www.pchalliance.org/continua-design-guidelines), Dec. 15, 2017 edition: We strive to follow these guidelines in 
  connection with:
    * Communication between measurement devices and patients' mobile devices
    * Communication between patients' mobile devices and servers collecting measurement results and questionnaire 
    responses
    * Communication between servers and central systems / archives
  - [Danish reference architectures for health IT](https://sundhedsdatastyrelsen.dk/da/rammer-og-retningslinjer/om-referencearkitektur-og-standarder/referencearkitekturer):
  We strive to follow the principles of these architectures. Note however, that some of these architectures date back
   to 2012/2013 and some standards and guidelines have evolved since. In such cases we 
   try to be true to the spirit of these architectures, while electing to go with updated/new standards and 
   guidelines. Most prominently this is the case wrt. the use of HL7 FHIR and the use of updated versions of Continua
    Design Guidelines 
  - [HL7 FHIR](http://hl7.org/fhir/): The project uses HL7 FHIR R4 version 4.0.0 in the following connections:
    * The basic data model shared between most GMK components (see 'Data model' below)
    * RESTful interfaces exposed by backend-for-frontends and input services on the backend (see [GMK Backend Architecture](HFS.md))
    * RESTful interfaces exposed by microservices on the backend (see [GMK Backend Architecture](HFS.md))
    * Payload data (body part) of asyncronous messages on the backend (see [GMK Backend Architecture](HFS.md))
  - HL7 CDA: On the backend we convert:
    * Measurement results from HL7 FHIR Observation resources to HL7 PHMR (_TODO: PHMR profile version used_)
    * Questionnaire responses from FHIR QuestionnaireResponse resources to HL7 QRD (_TODO: QRD profile version used_) 
  - [IEEE 11073-20601:2014](https://standards.ieee.org/standard/11073-20601-2014.html): Used in PHG modules for a 
  uniform, interoperable object model (Device Information Model, see [PHG documentation](https://www.4s-online.dk/PHG/core/latest/interface/index.html) for measurement devices 
  and measurements 
  
_Notes on Continua Design Guidelines:_

Personal Connected Health Alliance (PHCA) are in the process of 
profiling communication of questionnaire responses for HL7 FHIR. We follow this closely and currently implement 
reporting of questionnaire responses analogous to reporting of measurement results (see [fhir-input-service](https://bitbucket.org/4s/fhir-input-service/src/fhir_r4/docs/Main.md))) 

_Notes on questionnaires, CDA and FHIR:_

  - New questionnaires are created in FHIR Questionnaire resource format. The project will only create FHIR 
  Questionnaires that are compatible with the CDA format QFDD.
  - Questionnaire responses from the PHG device are received in FHIR QuestionnaireResponse resource format
  - Upon receipt FHIR QuestionnaireReponse resources are converted to the CDA format QRD. The QRD documents need to 
  reference a code for each question answered. The first version of this will be a reference to the linkId of the 
  question inside the FHIR Questionnaire that was answered by the QRD.

## Data model
In collaboration with suppliers of health IT solutions The Central Denmark Region has developed a profiling of the 
HL7 FHIR resources used in connection with remote patient monitoring systems. This profile forms the basic data model
 for most of the GMK components and can be found [here](https://issuetracker4s.atlassian.net/wiki/spaces/FDM/overview).
